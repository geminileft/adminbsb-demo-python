import os
from flask import Flask, send_from_directory, render_template, request
app = Flask(__name__, static_folder='static')

@app.route('/')
@app.route('/index.html')
def index():
    return render_template('index.html')

@app.route('/pages/typography.html')
def typography():
    return render_template('pages/typography.html')

@app.route('/pages/helper-classes.html')
def helper_classes():
    return render_template('pages/helper-classes.html')

@app.route('/pages/changelogs.html')
def changelogs():
    return render_template('pages/changelogs.html')

@app.route('/pages/widgets/cards/basic.html')
def widgets_cards_basic():
    return render_template('pages/widgets/cards/basic.html')

@app.route('/pages/widgets/cards/colored.html')
def widgets_cards_colored():
    return render_template('pages/widgets/cards/colored.html')

@app.route('/pages/widgets/cards/no-header.html')
def widgets_cards_no_header():
    return render_template('pages/widgets/cards/no-header.html')

@app.route('/pages/widgets/infobox/infobox-1.html')
def widgets_infobox_infobox_1():
    return render_template('pages/widgets/infobox/infobox-1.html')

@app.route('/pages/widgets/infobox/infobox-2.html')
def widgets_infobox_infobox_2():
    return render_template('pages/widgets/infobox/infobox-2.html')

@app.route('/pages/widgets/infobox/infobox-3.html')
def widgets_infobox_infobox_3():
    return render_template('pages/widgets/infobox/infobox-3.html')

@app.route('/pages/widgets/infobox/infobox-4.html')
def widgets_infobox_infobox_4():
    return render_template('pages/widgets/infobox/infobox-4.html')

@app.route('/pages/widgets/infobox/infobox-5.html')
def widgets_infobox_infobox_5():
    return render_template('pages/widgets/infobox/infobox-5.html')

@app.route('/pages/ui/alerts.html')
def ui_alerts():
    return render_template('pages/ui/alerts.html')

@app.route('/pages/ui/animations.html')
def ui_animations():
    return render_template('pages/ui/animations.html')

@app.route('/pages/ui/badges.html')
def ui_badges():
    return render_template('pages/ui/badges.html')

@app.route('/pages/ui/breadcrumbs.html')
def ui_breadcrumbs():
    return render_template('pages/ui/breadcrumbs.html')

@app.route('/pages/ui/buttons.html')
def ui_buttons():
    return render_template('pages/ui/buttons.html')

@app.route('/pages/ui/collapse.html')
def ui_collapse():
    return render_template('pages/ui/collapse.html')

@app.route('/pages/ui/colors.html')
def ui_colors():
    return render_template('pages/ui/colors.html')

@app.route('/pages/ui/dialogs.html')
def ui_dialogs():
    return render_template('pages/ui/dialogs.html')

@app.route('/pages/ui/icons.html')
def ui_icons():
    return render_template('pages/ui/icons.html')

@app.route('/pages/ui/labels.html')
def ui_labels():
    return render_template('pages/ui/labels.html')


@app.route('/pages/ui/list-group.html')
def ui_list_group():
    return render_template('pages/ui/list-group.html')


@app.route('/pages/ui/media-object.html')
def ui_media_object():
    return render_template('pages/ui/media-object.html')


@app.route('/pages/ui/modals.html')
def ui_modals():
    return render_template('pages/ui/modals.html')


@app.route('/pages/ui/notifications.html')
def ui_notifications():
    return render_template('pages/ui/notifications.html')


@app.route('/pages/ui/pagination.html')
def ui_pagination():
    return render_template('pages/ui/pagination.html')


@app.route('/pages/ui/preloaders.html')
def ui_preloaders():
    return render_template('pages/ui/preloaders.html')


@app.route('/pages/ui/progressbars.html')
def ui_progressbars():
    return render_template('pages/ui/progressbars.html')


@app.route('/pages/ui/range-sliders.html')
def ui_range_sliders():
    return render_template('pages/ui/range-sliders.html')



@app.route('/pages/ui/sortable-nestable.html')
def ui_sortable_nestable():
    mylist = [{'name':'Col 1', 'id': 'c1'},{'name':'Col 2', 'id': 'c2'},{'name':'Col 3', 'id': 'c3'},{'name':'Col 4', 'id': 'c4'},{'name':'Col 5', 'id': 'c5'},{'name':'__clone', 'id': '__clone'}]
    return render_template('pages/ui/sortable-nestable.html', list = mylist)


@app.route('/pages/ui/tabs.html')
def ui_tabs():
    return render_template('pages/ui/tabs.html')


@app.route('/pages/ui/thumbnails.html')
def ui_thumbnails():
    return render_template('pages/ui/thumbnails.html')


@app.route('/pages/ui/tooltips-popovers.html')
def ui_tooltips_popovers():
    return render_template('pages/ui/tooltips-popovers.html')

@app.route('/pages/ui/waves.html')
def ui_waves():
    return render_template('pages/ui/waves.html')

@app.route('/pages/forms/advanced-form-elements.html')
def forms_advanced_form_elements(**kwargs):
    return render_template(request.path)

@app.route('/pages/forms/basic-form-elements.html')
def forms_basic_form_elements(**kwargs):
    return render_template(request.path)


@app.route('/pages/forms/editors.html')
def forms_editors(**kwargs):
    return render_template(request.path)


@app.route('/pages/forms/form-examples.html')
def forms_form_examples(**kwargs):
    return render_template(request.path)


@app.route('/pages/forms/form-validation.html')
def forms_form_validation(**kwargs):
    return render_template(request.path)

@app.route('/pages/forms/form-wizard.html')
def forms_form_wizard(**kwargs):
    return render_template(request.path)

@app.route('/pages/tables/editable-table.html')
def tables_editable_table(**kwargs):
    return render_template(request.path)

@app.route('/pages/tables/jquery-datatable.html')
def tables_jquery_datatable(**kwargs):
    return render_template(request.path)

@app.route('/pages/tables/normal-tables.html')
def tables_normal_tables(**kwargs):
    return render_template(request.path)

@app.route('/pages/medias/carousel.html')
def medias_carousel(**kwargs):
    return render_template(request.path)

@app.route('/pages/medias/image-gallery.html')
def medias_image_gallery(**kwargs):
    return render_template(request.path)

@app.route('/pages/charts/chartjs.html')
def charts_chartjs(**kwargs):
    return render_template(request.path)

@app.route('/pages/charts/flot.html')
def charts_flot(**kwargs):
    return render_template(request.path)

@app.route('/pages/charts/jquery-knob.html')
def charts_jquery_knob(**kwargs):
    return render_template(request.path)

@app.route('/pages/charts/morris.html')
def charts_morris(**kwargs):
    return render_template(request.path)

@app.route('/pages/charts/sparkline.html')
def charts_sparkline(**kwargs):
    return render_template(request.path)

@app.route('/pages/maps/google.html')
def maps_google(**kwargs):
    return render_template(request.path)

@app.route('/pages/maps/jvectormap.html')
def maps_jvectormap(**kwargs):
    return render_template(request.path)

@app.route('/pages/maps/yandex.html')
def maps_yandex(**kwargs):
    return render_template(request.path)

@app.route('/pages/<p1>')
@app.route('/pages/<p1>/<p2>')
@app.route('/pages/<p1>/<p2>/<p3>')
@app.route('/pages/<p1>/<p2>/<p3>/<p4>')
@app.route('/pages/<p1>/<p2>/<p3>/<p4>/<p5>')
def pages(**kwargs):
    return render_template(request.path)

@app.route('/js/<path:filename>')
def serve_js_static(filename):
    return send_from_directory(os.path.join(app.static_folder, 'js'), filename)

@app.route('/css/<path:filename>')
def serve_css_static(filename):
    return send_from_directory(os.path.join(app.static_folder, 'css'), filename)

@app.route('/images/<path:filename>')
def serve_images_static(filename):
    return send_from_directory(os.path.join(app.static_folder, 'images'), filename)

@app.route('/plugins/<path:filename>')
def serve_plugins_static(filename):
    return send_from_directory(os.path.join(app.static_folder, 'plugins'), filename)

